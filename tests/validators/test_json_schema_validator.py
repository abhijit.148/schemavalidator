from pyproject.sources.local_file import LocalFile
from pyhocon import ConfigFactory

from pyproject.validators.json_schema_validator import JsonSchemaValidator


class TestJsonSchemaValidator:
    """
    In this test we don't want to test the validation capabilities of jsonschema package, because those are already
    tested by the package itself. We want to test that our JsonSchemaValidator class is capable of reading an input,
    and performing validation against each line in this input.
    """
    def test_validator(self, caplog):
        source_conf = { "input_file": "tests/resources/input.log"}
        source = LocalFile(ConfigFactory.from_dict(source_conf))

        validator_conf = {"schema_file": 'tests/resources/json_schema.json'}
        validator = JsonSchemaValidator(ConfigFactory.from_dict(validator_conf))

        validator.validate_source(source)

        assert "Encountered validation error on line 3: -1 is not of type 'string'" in caplog.text
        assert "Found 2 valid records" in caplog.text

