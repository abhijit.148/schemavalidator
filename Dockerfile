# This is not a production-ready Docker image

FROM continuumio/miniconda3:4.6.14

RUN apt-get update && apt-get install -y make

ADD . /opt/schemavalidator
WORKDIR /opt/schemavalidator

RUN make conda-env

CMD ["make", "run"]
