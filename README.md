# A Python Project for Validating JSON input

This is a simple project to validate JSON inputs against a given schema.


## Running with Docker
*If you don't want to use Docker, there is a section on running without docker, after the Implementation Details.*


This command will build the image specified in the `Dockerfile` and configure the volume mounts as described in 
`docker-compose.yml`.



```bash
docker-compose up
```

Tested on `Docker version 18.09.2`.

All code changes should take effect immediately. If there are changes made to the conda environment or to the image,
then you would need to run `build` step to re-build the image. 

```bash
docker-compose build
```

### Docker Cleanup

List all images and all containers (stopped, or running)

```bash
# List images
docker images
# Remove images
docker rmi <image-hash-or-name>

# List containers
docker ps -a
# Stop running containers
docker stop <container-hash-or-name>
# Delete stopped containers
docker rm <container-hash-or-name>
```


# Implementation Details

### Schema

Here is a sample for the JSON data that we are trying to parse and validate.

```json
{
    "id": "FB16866D-AE4D-416F-8848-122B07DA42F5",
    "received_at": "2018-01-30 18:13:52.221000",
    "anonymous_id": "0A52CDC6-DDDC-4F7D-AA24-4447F6AF2689",
    "context_app_version": "1.2.3",
    "context_device_ad_tracking_enabled": true,
    "context_device_manufacturer": "Apple",
    "context_device_model": "iPhone8,4",
    "context_device_type": "android",
    "context_locale": "de-DE",
    "context_network_wifi": true,
    "context_os_name": "android",
    "context_timezone": "Europe/Berlin",
    "event": "submission_success",
    "event_text": "submissionSuccess",
    "original_timestamp": "2018-01-30T19:13:43.383+0100",
    "sent_at": "2018-01-30 18:13:51.000000",
    "timestamp": "2018-01-30 18:13:43.627000",
    "user_id": "18946",
    "context_network_carrier": "o2-de",
    "context_device_token": null,
    "context_traits_taxfix_language": "en-DE"
}
```

## Sources

The `pyproject.sources` module has been created to provide  a plug-and-play style implementation where additional custom
sources can be implemented by extending the default `pyproject.sources.Source` abstract. 

To start with, I have created `pyproject.sources.local_file.LocalFile` as class that uses a text file on local disk as
the source of the data.


## Validators

Similar to `pyproject.sources`, `pyproject.validator` module was created to build a common interface for validating
data based on multiple implementations.

### JSON Schema Validator

This validator uses [JSON Schema](http://json-schema.org/learn/miscellaneous-examples.html) to validate the data, using
[python-jsonschema](https://python-jsonschema.readthedocs.io/en/stable/) package. 

The JSON is first de-serialized into a Python `dict`, and then validation is run on it by `jsonschema`.

The implementation is in: `pyproject.validator.json_schema_validator.JsonSchemaValidator`


### AVRO Schema Validator

The initial implementation of this project tried to use [fastavro](https://fastavro.readthedocs.io/en/latest/) to parse
and validate the JSON data. But due to a bug in this particular implementation of JSON Decorder in `fastavro`,it fails
if a JSON does not have all of the keys listed in the AVRO schema (even the optional ones).

The implementation has been left here for illustration purposes:
`pyproject.validator.avro_schema_validator.AvroSchemaValidator`


# Running without Docker

### Requirements

- Conda: tested on `conda 4.6.7` and `conda 4.5.11`
- Make (optional, but recommended): tested on `GNU Make 3.81`


### Setup

```
# Re-creates a dev environment and installs this project as a package
make conda-env-dev
```

If **not** using `make`:

```
# Creating an environment
conda env create --file environment.yaml
source activate pyproject-env

# Installing this project as a package
pip install -e .
```

### Tests 

```
# Runs pytest inside the conda environment
make test
```

If **not** using `make`:

```
source activate pyproject-env
pytest
```

### Running the program

```
# Runs the program as a module inside the conda environment
make run
```

If **not** using `make`:

```
source activate pyproject-env
python -m pyproject
```