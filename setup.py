#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import setuptools

# Check Python version
PYTHON_VERSION = 3, 7
if sys.version_info < PYTHON_VERSION:
    sys.exit("Python {}.{}+ is required.".format(*PYTHON_VERSION))


setuptools.setup(
    name="pyproject",
    version="0.1.0",
    description="A Test Project",
    url="https://github.com/abhijit148/pyproject",
    author="Abhijit Agarwal",
    author_email="abhijit.148@gmail.com",
    packages=setuptools.find_packages(exclude=("tests", )),
    long_description=open("README.md").read(),
)
