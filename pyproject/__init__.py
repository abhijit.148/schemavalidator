#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging.config
from pyhocon import ConfigFactory


logging.config.fileConfig('logging.conf')

conf = ConfigFactory.parse_file('config.hocon')
