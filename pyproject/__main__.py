#!/usr/bin/env python
# -*- coding: utf-8 -*-
from . import conf
from .sources.local_file import LocalFile
from .validators.json_schema_validator import JsonSchemaValidator
from .validators.avro_schema_validator import AvroSchemaValidator


def validate_avro():
    source_conf = conf.get("source")
    source = LocalFile(source_conf)

    # validator_conf = conf.get("validator.avro")
    # validator = AvroSchemaValidator(validator_conf)

    validator_conf = conf.get("validator.json")
    validator = JsonSchemaValidator(validator_conf)

    validator.validate_source(source)


def main():
    validate_avro()


if __name__ == "__main__":
    main()
