from abc import ABC, abstractmethod
from pyhocon import ConfigTree
from typing import Iterator, Any


class Source(ABC):
    """
    Generic interface for sources. Sources can be a file on local disk, a file on distributed FS,
    a database table, a Kafka stream.
    All sources should have a configuration source_conf. The source_conf can have a variable structure, depending
    on the type of source. For local file, it just needs an input file. For a Kafka stream, it would need info on the
    brokers, the topic name, ports, etc.
    The read method should implement an Iterable for reading the data from the source.
    """

    source_conf = None

    def __init__(self, source_conf: ConfigTree):
        self.source_conf = source_conf
        super().__init__()

    @abstractmethod
    def read(self) -> Iterator[Any]:
        pass
