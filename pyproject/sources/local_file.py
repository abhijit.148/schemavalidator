from . import Source
from typing import Iterator


class LocalFile(Source):
    """
    Reads a LocalFile from Disk
    """
    def read(self) -> Iterator[str]:
        with open(self.source_conf.get("input_file")) as f:
            for line in f:
                yield line
