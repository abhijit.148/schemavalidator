from pyproject.sources import Source
import json
from pyhocon import ConfigTree
from jsonschema import validate, ValidationError
import logging

from . import Validator

logger = logging.getLogger(__name__)


class JsonSchemaValidator(Validator):
    """
    In this validator, we are assuming that we will be passed a Source with string inputs.
    The validator decodes the JSON String into a python dict, and then performs validation.
    An alternative approach would be to create a Source class called JsonLocalFile, and let the parsing happen in the
    Source implementation. But here in this validator, we are also validating that the input is a valid JSON.
    """
    def __init__(self, validator_conf: ConfigTree):
        with open(validator_conf.get("schema_file")) as sf:
            self.json_schema = json.load(sf)

        super().__init__(validator_conf)

    def validate_source(self, source: Source):
        line_num = 1
        valid_num = 0

        for json_str in source.read():
            try:
                json_dict = json.loads(json_str)
                validate(json_dict, self.json_schema)
                valid_num += 1
            except ValidationError as e:
                logger.error(f"Encountered validation error on line {line_num}: {e}")
            except json.decoder.JSONDecodeError as e:
                logger.error(f"Encountered JSON parsing error on line {line_num}: {e}")
            finally:
                line_num += 1

        logger.info(f"Found {valid_num} valid records")
