from pyproject.sources import Source
import json
from pyhocon import ConfigTree
from fastavro import json_reader, parse_schema, validate
from fastavro._validate_common import ValidationError
import logging

from . import Validator


logger = logging.getLogger(__name__)


class AvroSchemaValidator(Validator):

    def __init__(self, validator_conf: ConfigTree):
        with open(validator_conf.get("schema_file")) as sf:
            self.avro_schema = parse_schema(json.load(sf))

        super().__init__(validator_conf)

    def validate_source(self, source: Source):
        avro_reader = json_reader(source.read(), self.avro_schema)

        line_num = 1
        valid_num = 0

        for record in avro_reader:
            try:
                if validate(record, self.avro_schema):
                    valid_num += 1
            except ValidationError as e:
                logger.error(f"Encountered validation error on line {line_num}: {e}")
            finally:
                line_num += 1

        logger.info(f"Found {valid_num} valid records")
