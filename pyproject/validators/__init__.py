from abc import ABC
from pyhocon import ConfigTree
from ..sources import Source


class Validator(ABC):
    def __init__(self, validator_conf: ConfigTree):
        self.validator_conf = validator_conf
        super().__init__()

    def validate_source(self, source: Source):
        pass
