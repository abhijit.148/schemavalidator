SHELL := /usr/bin/env bash


#######
# Help
#######

.DEFAULT_GOAL := help

define PRINT_HELP_PYSCRIPT
import re, sys

for line in sorted(sys.stdin):
    match = re.match(r'^([$()a-zA-Z_-]+):.*?## (.*)$$', line)
    if match:
        target, help = match.groups()
        print("%-20s %s" % (target, help))

endef
export PRINT_HELP_PYSCRIPT

help:
	@ python -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)


###################
# Conda Enviroment
###################
# Note: Python version is set in environment.yml by setting the Python version installed by Anaconda
MODULE_NAME = pyproject
CONDA_ENV_NAME ?= $(MODULE_NAME)-env
ACTIVATE_ENV = source activate $(CONDA_ENV_NAME)


.PHONY: conda-env
conda-env: $(CONDA_ENV_NAME)  ## Build the normal non-development conda enviroment
$(CONDA_ENV_NAME):
	conda env create -f environment.yaml
	$(ACTIVATE_ENV) && python -s -m pip install -e .


.PHONY: clean-conda-env
clean-conda-env:  ## Remove the conda environment and zip files
	conda env remove -n $(CONDA_ENV_NAME)


.PHONY: conda-env-dev
conda-env-dev: conda-env install-dev-reqs ## Build development conda enviroment


.PHONY: install-dev-reqs
install-dev-reqs: ## Install development requirements
	$(ACTIVATE_ENV) && python -s -m pip install -r requirements.dev.txt


##########
# Testing
##########

.PHONY: test
test: ## Run tests
	$(ACTIVATE_ENV) && pytest


##########
# RUN
##########

.PHONY: run
run: ## Run the program
	$(ACTIVATE_ENV) && python -m $(MODULE_NAME)
